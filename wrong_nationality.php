#!/usr/bin/php
<?PHP

require_once ( 'public_html/php/common.php' ) ;

$p571 = '571' ;
$p576 = '576' ;
$p569 = '569' ;
$p570 = '570' ;
$j = json_decode ( file_get_contents ( "$wdq_internal_url?q=".urlencode('claim[571] and claim[31:6256]')."&props=571,576" ) ) ;
$countries = array() ;
foreach ( $j->props->$p571 AS $v ) $countries[$v[0]]['start'] = $v[2] ;
foreach ( $j->props->$p576 AS $v ) $countries[$v[0]]['end'] = $v[2] ;

$out_header = '<html><head><meta charset="utf-8"></head><body>' ;
$out_header .= "<h1>Wrong nationalities on Wikidata</h1>" ;
$out_header .= "<p>These people have the wrong nationality, based on their birth/death dates, and the inception/dissolution time of the country.</p>" ;
$out_header .= "<p>To add more countries, set the inception/dissolution properties in the country items; this page is updated daily.</p>" ;
$out_header .= "<ul>" ;

function nice_date ( $d ) {
	return isset($d)?substr($d,8,10):'' ;
}

$total_misfits = 0 ;
$out = '' ;
foreach ( $countries AS $country => $cv ) {
	
	$q = array() ;
	if ( isset($cv['start']) ) $q[] = "BETWEEN[570,0,". $cv['start'] . "]" ;
	if ( isset($cv['end']) ) $q[] = "BETWEEN[569,". $cv['end'] . ",9999]" ;
	$url =  "$wdq_internal_url?q=".urlencode("claim[27:$country] AND (".implode(' OR ',$q)).")&props=569,570" ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( !is_object($j) ) continue ;
	if ( !isset($j->items) ) continue ;
	if ( count($j->items) == 0 ) continue ;
	
	$db = openDB ( 'wikidata' , 'wikidata' ) ;
	$sql = "SELECT * FROM wb_terms WHERE term_entity_id=$country AND term_entity_type='item' AND term_language='en' AND term_type='label'" ;
	$name = '' ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) $name = $o->term_text ;
	
	$people = array() ;
	foreach ( $j->props->$p569 AS $v ) $people[$v[0]]['born'] = $v[2] ;
	foreach ( $j->props->$p570 AS $v ) $people[$v[0]]['died'] = $v[2] ;
	if ( count($people) == 0 ) continue ;

	$out .= "<h2><a name='$country'></a>$name (<a target='_blank' href='//www.wikidata.org/wiki/Q$country'>Q$country</a>)</h2>\n" ;
	$out .= "<p>" ;
	$out .= isset($cv['start'])?substr($cv['start'],8,10):'' ;
	$out .= ' &ndash; ' ;
	$out .= isset($cv['end'])?substr($cv['end'],8,10):'' ;
	$out .= "</p>" ;

	$sql = "SELECT * FROM wb_terms WHERE term_entity_id IN (" . implode(',',$j->items) . ") AND term_entity_type='item' AND term_type='label'" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
	while($o = $result->fetch_object()) {
		if ( $o->term_language == 'en' or !isset($people[$o->term_entity_id]['name']) ) $people[$o->term_entity_id]['name'] = $o->term_text ;
	}

	$cnt = 0 ;
	$out .= "<ol>\n" ;
	foreach ( $people AS $pq => $p ) {
		$born = nice_date ( $p['born'] ) ;
		$died = nice_date ( $p['died'] ) ;
		if ( $born == '2000-01-01' and $died == '' ) continue ; // HARD HACK FIXME (20th century)
		if ( $born > $died ) continue ; // Aging backwards, never a good sign...
		$out .= "<li>" ;
		$out .= "<a target='_blank' href='//www.wikidata.org/wiki/Q$pq'>" ;
		if ( isset ( $p['name'] ) ) $out .= $p['name'] ;
		else $out .= "Q$pq" ;
		$out .= "</a> (" ;
		$out .= $born ;
		$out .= ' &ndash; ' ;
		$out .= $died ;
		$out .= ")</li>\n" ;
		$cnt++ ;
	}
	$out .= "</ol>\n" ;
	$total_misfits += $cnt ;
	
	$out_header .= "<li><a href='#$country'>$name</a> (" . $cnt . " people)</li>" ;

	$db->close() ;
}

$out_header .= "</ul>" ;
$out_header .= "<p>Total : $total_misfits people</p><hr/>" ;

$out = $out_header . $out . '</body></html>' ;

$fh = fopen ( "public_html/wrong_nationality.html" , 'w' ) ;
fwrite ( $fh , $out ) ;
fclose ( $fh ) ;
?>