#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$db = openDB ( 'wikidata' , 'wikidata' , true ) ;

$name2q = array (
	'cultivar' => 'Q4886' ,
	'species' => 'Q7432' ,
	'genus' => 'Q34740' ,
	'family' => 'Q35409' ,
	'order' => 'Q36602' ,
	'kingdom' => 'Q36732' ,
	'class' => 'Q37517' ,
	'phylum' => 'Q38348' ,
	'subspecies' => 'Q68947' ,
	'domain' => 'Q146481' ,
	'tribe' => 'Q227936' ,
	'form' => 'Q279749' ,
	'division' => 'Q334460' ,
	'subvariety' => 'Q630771' ,
	'cryptic species complex' => 'Q765940' ,
	'variety' => 'Q767728' ,
	'subphylum' => 'Q1153785' ,
	'nothospecies' => 'Q1306176' ,
	'superspecies' => 'Q1783100' ,
	'infraclass' => 'Q2007442' ,
	'superfamily' => 'Q2136103' ,
	'infraphylum' => 'Q2361851' ,
	'subfamily' => 'Q2455704' ,
	'subkingdom' => 'Q2752679' ,
	'infraorder' => 'Q2889003' ,
	'cohorte' => 'Q2981883' ,
	'series' => 'Q3025161' ,
	'infrakingdom' => 'Q3150876' ,
	'section' => 'Q3181348' ,
	'subgenus' => 'Q3238261' ,
	'branch' => 'Q3418438' ,
	'subdomain' => 'Q3491996' ,
	'subdivision' => 'Q3491997' ,
	'superclass' => 'Q3504061' ,
	'forma specialis' => 'Q3825509' ,
	'subtribe' => 'Q3965313' ,
	'superphylum' => 'Q3978005' ,
	'group' => 'Q4150646' ,
	'infracohort' => 'Q4226087' ,
	'form' => 'Q5469884' ,
	'infrafamily' => 'Q5481039' ,
	'subclass' => 'Q5867051' ,
	'suborder' => 'Q5867959' ,
	'superorder' => 'Q5868144' ,
	'subsection' => 'Q5998839' ,
	'nothogenus' => 'Q6045742' ,
	'magnorder' => 'Q6054237' ,
	'supercohort' => 'Q6054425' ,
	'infralegion' => 'Q6054535' ,
	'sublegion' => 'Q6054637' ,
	'superlegion' => 'Q6054795' ,
	'parvorder' => 'Q6311258' ,
	'grandorder' => 'Q6462265' ,
	'legion' => 'Q7504331' ,
	'mirorder' => 'Q7506274' ,
	'subcohorte' => 'Q7509617' ,
	'species group' => 'Q7574964' ,
	'epifamily' => 'Q10296147' ,
	'subsection' => 'Q10861375' ,
	'section' => 'Q10861426' ,
	'subseries' => 'Q13198444' ,
	'subform' => 'Q13202655' ,
	'supertribe' => 'Q14817220' ,
	'superkingdom' => 'Q19858692' ,
	'subterclass' => 'Q21061204' ,
	'hyporder' => 'Q21074316' ,
	'any' => 'Q0'
) ;


$base = 'remaining_taxa' ;
#$base = 'manual_species' ;

$rows = explode ( "\n" , file_get_contents ( "$base.tab" ) ) ;

$fh1 = fopen ( "$base.add" , 'w' ) ;
$fh2 = fopen ( "$base.create" , 'w' ) ;

$p225 = '225' ;
$taxon_names = array() ;
$url = "$wdq_internal_url?q=" . urlencode('claim[225] and link[specieswiki]') . "&props=225" ;
$j = json_decode ( file_get_contents ( $url ) ) ;
foreach ( $j->props->$p225 AS $k => $v ) $taxon_names[$v[2]] = $v[0] ;

foreach ( $rows AS $row ) {
	$parts = explode ( "\t" , $row ) ;
	$type = trim($parts[0]) ;
	$page = $parts[1] ;
	
	if ( preg_match ( '/\(/' , $page ) ) continue ;
	if ( preg_match ( '/unspecified/i' , $page ) ) continue ;
	if ( preg_match ( '/\bgenera\b/i' , $page ) ) continue ;
	if ( preg_match ( '/\bdivision\b/i' , $page ) ) continue ;
	if ( preg_match ( '/\bfamily\b/i' , $page ) ) continue ;
	if ( preg_match ( '/\bgroup\b/i' , $page ) ) continue ;
	
	if ( preg_match ( '/\//' , $type  ) ) continue ;
	if ( preg_match ( '/unranked/' , $type  ) ) continue ;
	
	$type = preg_replace ( '/^.+>/' , '' , $type ) ;
	$type = preg_replace ( '/[\(\)&\?].+$/' , '' , $type ) ;

	$type = preg_replace ( '/famil.*$/' , 'family' , $type ) ;
	$type = preg_replace ( '/class.*$/' , 'class' , $type ) ;
	$type = preg_replace ( '/^trib.*$/' , 'tribe' , $type ) ;
	$type = preg_replace ( '/ordo$/' , 'order' , $type ) ;
	
	if ( !isset($name2q[$type]) ) continue ;

	if ( isset($taxon_names[$page]) ) {
		continue ;
	}
	
	$q_type = preg_replace ( '/\D/' , '' , $name2q[$type] ) ;
	$query = "string[225:\"$page\"]" ;
	if ( $q_type != '0' ) $query .= " and claim"."[105:$q_type]" ;
	$url = "$wdq_internal_url?q=" . urlencode($query) ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( count($j->items) > 1 ) {
		error_log ( "BAD TAXON: $page\n" ) ;
		continue ;
	}

	if ( count($j->items) == 0 ) {
		$skip = false ;
		$sql = "select * from wb_terms WHERE term_text='$page' LIMIT 1" ;
		if ( !$db->ping() ) $db = openDB ( 'wikidata' , 'wikidata' , true ) ;
		if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'." 1\n$sql\n\n");
		if($o = $result->fetch_object()) $skip = true ;
		if ( $skip ) continue ;
		$out = "CREATE\n" ;
		if ( $q_type != '0' ) $out .= "LAST\tP105\tQ$q_type\n" ;
		$out .= "LAST\tLen\t\"$page\"\nLAST\tSspecieswiki\t\"$page\"\nLAST\tP31\tQ16521\nLAST\tP225\t\"$page\"\n" ;
		fwrite ( $fh2 , $out ) ;
		continue ;
	}
	
	$q = $j->items[0] ;

	$skip = false ;
	$sql = "select * from wb_items_per_site where ips_site_id='specieswiki' AND ips_item_id=$q LIMIT 1" ;
	if ( !$db->ping() ) $db = openDB ( 'wikidata' , 'wikidata' , true ) ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'." 2\n$sql\n\n");
	if($o = $result->fetch_object()) $skip = true ;
	if ( $skip ) continue ;
	
	fwrite ( $fh1 , "Q$q\tSspecieswiki\t\"$page\"\n" ) ;

}

fclose ( $fh1 ) ;
fclose ( $fh2 ) ;

?>