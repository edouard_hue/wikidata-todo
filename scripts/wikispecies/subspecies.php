#!/usr/bin/php
<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR); // E_ALL|

require_once ( '../../public_html/php/common.php' ) ;

$taxa_tmp = explode ( "\n" , file_get_contents ( 'taxa.tab' ) ) ;
$taxa = array() ;
foreach ( $taxa_tmp AS $t ) {
	if ( !preg_match ( '/^[A-Z][a-z]+ [a-z]{3,} [a-z]{3,}$/' , $t ) ) continue ;
	$taxa[$t] = $t ;
}
unset ( $taxa_tmp ) ;

$has_sw = array() ;
$db = openDB ( 'wikidata' , 'wikidata' , true ) ;
$sql = "SELECT ips_site_page FROM wb_items_per_site WHERE ips_site_id='specieswiki'" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
while($o = $result->fetch_object()){
	$has_sw[] = 1 ;
	if ( isset ( $taxa[$o->ips_site_page] ) ) unset ( $taxa[$o->ips_site_page] ) ;
}
$taxa = array_values ( $taxa ) ;

$fh = fopen ( "subspecies.out" , 'w' ) ;

$ref = "\tS143\tQ13679" ;
foreach ( $taxa AS $t ) {
	$url = "http://wdq.wmflabs.org/api?q=" . urlencode("string[225:\"$t\"] and claim[105:68947]") ;
	$j = json_decode ( file_get_contents ( $url ) ) ;
	if ( count($j->items) > 1 ) continue ; // Wat?
	$s = '' ;
	if ( count($j->items) == 1 ) {
		$q = $j->items[0] ;
		if ( isset($has_sw["$q"]) ) continue ; // Wat?
		$s = "Q$q\tSspecieswiki\t\"$t\"\n" ;
	} else {
		$s = "CREATE\n" ;
		$s .= "LAST\tLen\t\"$t\"\n" ;
		$s .= "LAST\tP225\t\"$t\"\n" ;
		$s .= "LAST\tP31\tQ16521$ref\n" ;
		$s .= "LAST\tP105\tQ68947\n" ;
		$s .= "LAST\tSspecieswiki\t\"$t\"\n" ;
	}
	fwrite ( $fh , $s ) ;
}

fclose ( $fh ) ;

?>