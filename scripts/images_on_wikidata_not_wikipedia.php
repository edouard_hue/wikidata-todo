#!/usr/bin/php
<?PHP

require_once ( '/data/project/wikidata-todo/public_html/php/common.php' ) ;

$langs = array ( 'de' , 'es' , 'it' , 'ru' , 'nl' , 'ca' , 'en' , 'cy' ) ;
#$langs = array ( 'cy' ) ; # TESTING

$dir = '/data/project/wikidata-todo/public_html/wp_no_image' ;
$head = "<!doctype html>\n<html><head><meta charset='utf-8'></head><body><p><a href='/wikidata-todo/wp_no_image'>All wikis</a></p><p>Last update: " . date('r') . "</p>" ;
$head .= "<p>Pages are listed if they have an image on Wikidata, but no \"page image\", as determined by Wikipedia. If the Wikipedia page already has an image in the header section, you can complain <a target='_blank' href='https://phabricator.wikimedia.org/'>here</a>.</p>" ;
$foot = "</body></html>" ;

function cmp ( $a , $b ) {
	return count($b) - count($a) ;
}

$counts = array() ;
foreach ( $langs AS $lang ) {
	$project = 'wikipedia' ;
	$wiki = $lang.'wiki' ;
	$db = openDBwiki ( $wiki , true ) ;
	
	$data_tmp = array() ;
	
	$counts[$wiki] = 0 ;
	$sql = 'SELECT local_page.page_title AS lp_title,pp1.pp_value AS q FROM page local_page,page_props pp1
	WHERE local_page.page_is_redirect=0 AND local_page.page_namespace=0 AND NOT EXISTS (SELECT * FROM page_props pp2 WHERE local_page.page_id=pp2.pp_page AND pp2.pp_propname IN ("page_image","page_image_free") ) 
	AND local_page.page_id=pp1.pp_page AND pp1.pp_propname="wikibase_item" 
	AND EXISTS (SELECT * FROM wikidatawiki_p.page wd_page,wikidatawiki_p.pagelinks WHERE wd_page.page_namespace=0 AND wd_page.page_title=pp1.pp_value AND pl_from=wd_page.page_id AND pl_namespace=120 AND pl_title="P18")' ;

	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()){
		$io = 'other' ;
		$q = $o->q ;
		if ( isset ( $data_tmp[$q] ) ) continue ;
		$data_tmp[$q] = $o->lp_title ;
		$counts[$wiki]++ ;
	}

	$data = array() ;
	$data_tmp2 = array_keys ( $data_tmp ) ;
	while ( count($data_tmp2) > 0 ) {
		$tmp = array() ;
		while ( count($data_tmp2) > 0 && count($tmp) < 100 ) $tmp[] = array_pop ( $data_tmp2 ) ;
		if ( count($tmp) == 0 ) continue ; // Paranoia
		$sparql = "SELECT ?q ?image ?ioLabel { VALUES ?q { wd:" . implode ( ' wd:' , $tmp ) . " } ?q wdt:P18 ?image OPTIONAL { ?q wdt:P31 ?io } SERVICE wikibase:label { bd:serviceParam wikibase:language \"$lang\" } }" ;
		$j = getSPARQL ( $sparql ) ;
		if ( !isset($j) ) continue ;
		if ( !isset($j->results) ) continue ;
		if ( !isset($j->results->bindings) ) continue ;
		foreach ( $j->results->bindings AS $v ) {
			$io = 'unknown' ;
			if ( isset($v->ioLabel) and $v->ioLabel->type == 'literal' ) $io = $v->ioLabel->value ;
			$q = preg_replace ( '/^.+\//' , '' , $v->q->value ) ;
			$img = preg_replace ( '/^.+\//' , '' , $v->image->value ) ;
//			print "$q\t$img\t$io\n" ;
			if ( !isset($data_tmp[$q]) ) continue ;
			if ( isset($data[$io][$q]) ) continue ;
			$data[$io][$q] = array ( 'q' => $q , 'image' => $img , 'page' => $data_tmp[$q] ) ;
		}
	}
	uasort ( $data , 'cmp' ) ;
	
//	print_r ( $data ) ; exit ( 0 ) ;



	$fn = "$dir/$wiki.html" ;
	$fh = fopen ( "$fn.tmp" , 'w' ) ;
	fwrite ( $fh , $head ) ;
	fwrite ( $fh , "<p>Total: " . $counts[$wiki] . " pages with image candidates.</p>" ) ;

	foreach ( $data AS $io => $pages ) {
		fwrite ( $fh , "<h2>$io</h2><ol>" ) ;
		foreach ( $pages AS $p ) {
			$page = $p['page'] ;
			$q = $p['q'] ;
			$img = $p['image'] ;
			$s = "<li><a target='_blank' href='//$lang.$project.org/wiki/".urlencode($page)."'>".str_replace('_',' ',$page)."</a>" ;
			$s .= " [<a target='_blank' href='https://www.wikidata.org/wiki/$q'>$q</a>]" ;
			$s .= " : " . urldecode($img) . " [<a target='_blank' href='http://commons.wikimedia.org/wiki/File:$img'>Commons</a>]" ;
			$s .= "</li>" ;
			fwrite ( $fh , $s ) ;
		}
		fwrite ( $fh , "</ol>" ) ;
	}

	fwrite ( $fh , $foot ) ;
	fclose ( $fh ) ;
	rename ( "$fn.tmp" , $fn ) ;
}

$fh = fopen ( "$dir/index.html" , 'w' ) ;
fwrite ( $fh , $head ) ;
foreach ( $langs AS $lang ) {
	$wiki = $lang.'wiki' ;
	fwrite ( $fh , "<li><a href='$wiki.html'>$wiki</a> (" . $counts[$wiki] . " pages)</li>" ) ;
}
fwrite ( $fh , $foot ) ;
fclose ( $fh ) ;


?>
