<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','500M');

include_once ( "php/common.php" ) ;
include_once ( "php/wikidata.php" ) ;

$wil = new WikidataItemList ;

// Load target tools
$wil->loadItem ( 'Q43649390' ) ;
$tools = array() ;
$i = $wil->getItem ( 'Q43649390' ) ;
$claims = $i->getClaims ( 'P3303' ) ;
foreach ( $claims AS $c ) {
	if ( !isset($c->qualifiers) ) continue ;
	if ( !isset($c->qualifiers->P361) ) continue ;
	$url_pattern = $c->mainsnak->datavalue->value ;
	foreach ( $c->qualifiers->P361 AS $qual ) {
		$qq = $qual->datavalue->value->id ;
		$wil->loadItem ( $qq ) ;
		$iq = $wil->getItem ( $qq ) ;
		if ( !isset ( $iq ) ) continue ;
		
		// Get sortest English name or alias
		$names = $iq->getAllAliases() ;
		if ( isset($names['en']) ) $names = $names['en'] ;
		else $names = [] ;
		$l = $iq->getLabel('en') ;
		foreach ( $names AS $n ) {
			if ( strlen($l) > strlen($n) ) $l = $n ;
		}
		$l = str_replace ( ' ' , '_' , trim ( strtolower ( $l ) ) ) ;
		
		// Store pattern
		$tools[$l] = $url_pattern ;
	}
}
ksort ( $tools ) ;


$no_autoload = trim ( get_request ( 'no_autoload' , '' ) ) ;
$prop = preg_replace ( '/\D/' , '' , get_request ( 'prop' , '' ) ) ;
$value = trim ( get_request ( 'value' , '' ) ) ;
$quick = trim ( get_request ( 'quick' , '' ) ) ;
$quick_orig = $quick ;

$j = '' ;

if ( $prop != '' and $value != '' ) {
	$sparql = "SELECT ?q { ?q wdt:P$prop \"$value\" }" ;
	$j = (object) array ( 'items' => getSPARQLitems($sparql) ) ;
} else if ( $quick != '' ) {
	$quick = explode ( ':' , $quick , 2 ) ;
	$propname = trim(strtolower(preg_replace('/[ _].*$/','',$quick[0]))) ;
	$value = trim(str_replace('"','',$quick[1])) ;
	$sparql = "SELECT ?property { ?property a wikibase:Property . ?property rdfs:label ?label FILTER (lang(?label) = \"en\") FILTER ( STRSTARTS(lcase(?label),\"$propname\") )}" ;
	$pj = getSPARQLitems ( $sparql , 'property' ) ;
	if ( count($pj) == 1 ) {
		$prop = preg_replace ( '/^.+entity\/P/' , '' , $pj[0] ) ;
		$sparql = "SELECT ?q { ?q wdt:P$prop \"$value\" }" ;
		$j = (object) array ( 'items' => getSPARQLitems($sparql) ) ;
		
	}
#	print $sparql ; print_r ( $pj ) ; exit(0) ;
}

if ( $j != '' and $no_autoload == '' ) {
	if ( count ( $j->items ) == 1 ) {
		$server = 'www.wikidata.org' ;
		$page = 'Q' . $j->items[0] ;
		$url = "https://$server/wiki/" . myurlencode($page) ;
		
		$project = strtolower ( trim ( get_request ( 'project' , '' ) ) ) ;
		if ( isset($tools[$project]) ) {
			$url = str_replace ( '$1' , $page , $tools[$project] ) ;
		} else if ( $project != '' ) {
			$wil->loadItem ( $page ) ;
			$i = $wil->getItem ( $page ) ;
			if ( isset($i) ) {
				$sl = $i->getSitelink ( $project ) ;
				if ( isset($sl) ) {
					$page = $sl ;
					$server = getWebserverForWiki ( $project ) ;
				}
			}
			$url = "https://$server/wiki/" . myurlencode($page) ;
		}
		
		header('Content-type: text/html; charset=UTF-8'); // UTF8 test
		header("Cache-Control: no-cache, must-revalidate");
		print "<html>\n<meta HTTP-EQUIV='REFRESH' content='0; url=$url'></head></html>" ;
		exit ( 0 ) ;
	}
}

print get_common_header ( '' , 'Wikidata Resolver' ) ;

print "<div class='lead'>Redirect to a Wikidata item, based on a string value for a property.</div>" ;

if ( $j != '' and count ( $j->items ) > 1 ) {
	print "<div>Multiple Wikidata items with P$prop:\"$value\" :<ol>" ;
	foreach ( $j->items AS $i ) {
		print "<li><a href='//www.wikidata.org/wiki/Q$i' target='_blank'>Q$i</a></li>" ;
	}
	print "</ol>" ;
}

print "<form method='get' class='form inline-form form-inline' action='?' style='margin-bottom:20px;'>
<input class='form-element' type='text' name='prop' placeholder='Property (e.g. P227)' value='$prop' size='30' />
<input class='form-element' type='text' name='value' placeholder='Value (e.g. 119186187)' value='$value' size='80' />
<input type='submit' value='Find Wikidata item' class='btn btn-primary' />
Use alternative wiki project <input class='form-element' type='text' name='project' placeholder='e.g. enwiki' />
<label><input type='checkbox' class='form-element' name='no_autoload' value='1' " . ($no_autoload==''?'':'checked ') . "/> Just show value instead of loading Wikidata/Wikipedia</label>
</form>" ;

print "<form method='get' class='form inline-form form-inline' action='?'>
<div>
Quick lookup <input class='form-element' type='text' id='quick' name='quick' placeholder='e.g. VIAF:12307054' value='$quick_orig' size='120' />
Use alternative wiki project <input class='form-element' type='text' name='project' placeholder='e.g. enwiki' />
<label><input type='checkbox' class='form-element' name='no_autoload' value='1' " . ($no_autoload==''?'':'checked ') . "/> Just show value instead of loading Wikidata/Wikipedia</label>
</div>
<div><input type='submit' value='Find Wikidata item' class='btn btn-primary' /></div>
<div>(<a href='#' onclick='$(\"#quick\").val(\"VIAF:12307054\");return false'>example</a>)</div>
</form>" ;

print "<p><i>Note:</i> Alternative wiki project may also be: <tt>" ;
print implode ( "</tt>, <tt>" , array_keys ( $tools ) ) ;
print "</tt>.</p>" ;

//print "TESTING, IGNORE<pre>" ; print_r ( $tools ) ; print "</pre>" ;

print get_common_footer() ;

?>