<?PHP

require_once ( 'php/common.php' ) ;

$wiki = get_request ( 'wiki' , 'enwiki' ) ;
$server = getWebserverForWiki ( $wiki ) ;

$sparql = "SELECT ?animal ?file ?article {
  ?animal wdt:P31 wd:Q16521 .
  ?animal wdt:P51 ?file .
  ?article	schema:about ?animal ; schema:isPartOf <https://$server/>
  }" ;
$j = getSPARQL ( $sparql ) ;

$data = array () ;
foreach ( $j->results->bindings AS $row ) {
	if ( $row->animal->type != 'uri' ) continue ;
	if ( $row->file->type != 'uri' ) continue ;
	if ( $row->article->type != 'uri' ) continue ;
	$page = preg_replace ( '/^.+\/wiki\//' , '' , urldecode($row->article->value) ) ;
	$data[$page] = (object) array (
		'page' => $page ,
		'file' => preg_replace ( '/^.+\/Special:FilePath\//' , '' , urldecode($row->file->value) ) ,
		'q' => preg_replace ( '/^.+\/entity\//' , '' , urldecode($row->animal->value) )
	) ;
}

$pages = array() ;
$db = openDBwiki ( $wiki ) ;
foreach ( $data AS $page => $d ) $pages[$page] = $db->real_escape_string ( $page ) ;

if ( count($pages) > 0 ) {
	$sql = "SELECT DISTINCT page_title FROM page WHERE page_namespace=0 AND page_title IN ('" . implode("','",$pages) . "')" ;
	$sql .= " AND NOT EXISTS (SELECT * FROM imagelinks WHERE il_from=page_id AND (il_to LIKE '%.flac' OR il_to LIKE '%.oga' OR il_to LIKE '%.ogg'))" ;
	if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n");
	while($o = $result->fetch_object()){
		$page = $o->page_title ;
		$data[$page]->missing = true ;
	}
}

print get_common_header() ;
print "<h1>Animals with sound on Wikidata, but no sound on $wiki</h1>" ;

print "<form method='get' class='form-inline'>
<div class='form-group'>
<input type='text' name='wiki' value='$wiki' class='form-control'/>
<input type='submit' value='Use this wiki' class='btn btn-primary-outline' />
</div>
</form>" ;

ksort ( $data ) ;

$cnt = 0 ;
print "<div style='overflow:auto;margin-top:1em'>" ;
print "<table class='table table-condensed table-striped'>" ;
foreach ( $data AS $page => $d ) {
	if ( !isset($d) or !isset($d->missing) or !$d->missing ) continue ;
	$cnt++ ;
	print "<tr>" ;
	print "<td style='font-family:Courier;text-align:right'>$cnt</td>" ;
	print "<td><a href='https://$server/wiki/" . myurlencode($page) . "' target='_blank'>" . str_replace('_',' ',$page) . "</a></td>" ;
	print "<td><pre style='font-size:8pt;'>" . str_replace ( '_' , ' ' , $d->file ) . "</pre></td>" ;
	print "</tr>" ;
}
print "</table></div>" ;

/*
print "<pre>" ;
print_r ( $data ) ;
print "</pre>" ;
*/

?>