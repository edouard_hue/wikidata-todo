<?PHP

error_reporting(E_ERROR|E_CORE_ERROR|E_COMPILE_ERROR);
ini_set('display_errors', 'On');
ini_set('memory_limit','3500M');
set_time_limit ( 60 * 10 ) ; // Seconds
include_once ( 'php/common.php' ) ;

$db = openDB ( 'wikidata' , 'wikidata' ) ;

print get_common_header ( '' , 'True Duplicates' ) ;

print "<div class='lead'>This tool shows Wikidata items that link to the same Wiki(m|p)edia page. Those are a glitch in the matrix and need to be ... resolved.</div>" ;
print "<p>This may take  minute or so...</p>" ;
myflush();

print "<ol>" ;
//$sql = "SELECT ips_site_id,ips_site_page,count(distinct ips_item_id) AS cnt,group_concat(DISTINCT ips_item_id separator '|') AS items FROM wb_items_per_site GROUP BY ips_site_id,ips_site_page having cnt>1" ;
/*$sql = "SELECT ips_site_id,ips_site_page,ips_item_id FROM wb_items_per_site" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
$d = array() ;
while($o = $result->fetch_object()){
	$d[$o->ips_site_id.':'.$o->ips_site_page] .= $o->ips_item_id . '|' ;
}
*/

$sql = "SELECT ips_site_id,ips_site_page,ips_item_id FROM wb_items_per_site ORDER BY ips_site_id,ips_site_page" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']');
$d = array() ;
$tmp = array('','','') ;
while($o = $result->fetch_object()){
	if ( $o->ips_site_id != $tmp[0][0] or $o->ips_site_page != $tmp[0][1] ) {
		if ( count ( $tmp ) > 1 ) {
			$page = $tmp[0][0] . ':' . $tmp[0][1] ;
			$d[$page] = array() ;
			foreach ( $tmp AS $k => $v ) $d[$page][] = $v[2] ;
		}
		$tmp = array ( array ( $o->ips_site_id , $o->ips_site_page , $o->ips_item_id ) ) ;
	} else {
		$tmp[] = array ( $o->ips_site_id , $o->ips_site_page , $o->ips_item_id ) ;
	}
}

foreach ( $d AS $page => $items ) {
	print "<li>$page is linked from " ;
	foreach ( $items AS $k => $q ) {
		if ( $k > 0 ) print ", " ;
		print "<a target='_blank' href='//www.wikidata.org/wiki/Q$q'>$q</a>" ;
	}
	print "</li>" ;
}
print "</ol>" ;

print get_common_footer() ;

?>