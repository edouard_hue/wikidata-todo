<?PHP

//error_reporting(E_ERROR|E_CORE_ERROR|E_ALL|E_COMPILE_ERROR);

require_once ( 'php/common.php' ) ;
require_once ( 'php/wikidata.php' ) ;

header('Content-type: text/plain; charset=UTF-8');
header("Cache-Control: no-cache, must-revalidate");

$wiki = get_request ( 'wiki' , '' ) ;
$article = get_request ( 'article' , '' ) ;
$testing = get_request('testing',0)*1 ;
$max_cat_size = 500 ;
$max_items = 1000 ;
$min_percent = 2 ;



$db = openDBwiki ( $wiki ) ;
$a = $db->real_escape_string ( str_replace ( ' ' , '_' , $article ) ) ;

$cats = array() ;
$sql = "select distinct cl_to from categorylinks,page WHERE page_namespace=0 and page_title='$a' and cl_from=page_id" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	if ( preg_match ( '/:/' , $o->cl_to ) ) continue ; // No maintenance cats
	$cats[] = $o->cl_to ;
}

$to_check = array() ;
foreach ( $cats AS $c ) {
	$pages = getPagesInCategory ( $db , $c , 0 , 0 , true ) ;
	if ( count($pages) >= $max_cat_size ) continue ;
	foreach ( $pages AS $p ) $to_check[$p] = $db->real_escape_string ( str_replace ( '_' , ' ' , $p ) ) ;
}

$db = openDB ( 'wikidata' , 'wikidata' ) ;
$sql = "select distinct ips_item_id from wb_items_per_site WHERE ips_site_id='" . $db->real_escape_string($wiki) . "' and ips_site_page IN ('" . implode("','",$to_check) . "')" ;
if(!$result = $db->query($sql)) die('There was an error running the query [' . $db->error . ']'."\n$sql\n\n");
while($o = $result->fetch_object()){
	$items[] = 'Q' . $o->ips_item_id ;
}

shuffle ( $items ) ;
$items = array_slice ( $items , 0 , $max_items ) ;

$wil = new WikidataItemList ;
$wil->loadItems ( $items ) ;

$pvc = array() ;
foreach ( $items AS $iq ) {
	$i = $wil->getItem($iq) ;
	if ( !isset($i) ) continue ;
	$props = $i->getProps() ;
	foreach ( $props AS $p ) {
		$claims = $i->getClaims ( $p ) ;
		foreach ( $claims AS $c ) {
			$t = $i->getTarget ( $c ) ;
			if ( $t === false ) continue ;
			$pvc[$p][$t]++ ;
		}
	}
}

$props = array() ;
$cutoff = count($items) * $min_percent / 100 ;
if ( $cutoff < 5 ) $cutoff = 5 ;
foreach ( $pvc AS $p => $v0 ) {
	foreach ( $v0 AS $q => $cnt ) {
		if ( $cnt < $cutoff ) unset ( $pvc[$p][$q] ) ;
	}
	if ( count($pvc[$p]) == 0 ) unset ( $pvc[$p] ) ;
	else {
		arsort ( $pvc[$p] , SORT_NUMERIC ) ;
		$props[$p] = $p ;
	}
}

asort ( $pvc ) ;

$labels = array() ;

$to_load = $props ;

foreach ( $pvc AS $p => $v0 ) {
	foreach ( $v0 AS $q => $cnt ) $to_load[] = $q ;
}

$wil->loadItems ( $to_load ) ;
foreach ( $to_load AS $x ) {
	$i = $wil->getItem($x) ;
	if ( !isset($i) ) continue ;
	$labels[$x] = $i->getLabel() ;
}


$out = array (
	'data' => $pvc ,
	'labels' => $labels
) ;
if ( isset($_REQUEST['callback']) ) print $_REQUEST['callback'] . "(" ;
print json_encode ( $out ) ;
if ( isset($_REQUEST['callback']) ) print ");" ;

?>